#include "mewie.h"

enum NyashcaChar {
	MEWIE_CHAR_NULL = '\0',
	MEWIE_CHAR_EOF = EOF,
	MEWIE_CHAR_BACKSLASH = '\\',
	MEWIE_CHAR_SPACE = ' ',
	MEWIE_CHAR_TAB = '\t',
	MEWIE_CHAR_CR = '\r',
	MEWIE_CHAR_LF = '\n',
	MEWIE_CHAR_SEMICOLON = ';',
	MEWIE_CHAR_HASH = '#',
	MEWIE_CHAR_OPENING_PARENTHESIS = '(',
	MEWIE_CHAR_CLOSING_PARENTHESIS = ')',
	MEWIE_CHAR_QUOTE = '\'',
	MEWIE_CHAR_PLUS = '+',
	MEWIE_CHAR_MINUS = '-',
	MEWIE_CHAR_POINT = '.',
	MEWIE_CHAR_DOUBLE_QUOTE = '"',
	MEWIE_CHAR_ESCAPE_FROM_CR = 'r',
	MEWIE_CHAR_ESCAPE_FROM_LF = 'n',
	MEWIE_CHAR_ESCAPE_FROM_TAB = 't',
	MEWIE_CHAR_ESCAPE_FROM_CHAR = 'h'
};

const size_t MEWIE_CHAR_DIGITS_COUNT = 2;

const char *MEWIE_WORD_FUNCTION_START = "do";
const char *MEWIE_WORD_BRANCH_START = "if";
const char *MEWIE_WORD_BRANCH_AGAIN = "else-if";
const char *MEWIE_WORD_BRANCH_OTHERWISE = "else";
const char *MEWIE_WORD_LOOP_START = "while";
const char *MEWIE_WORD_BLOCK_END = "end";
const char *MEWIE_WORD_HEX_DIGITS = "0123456789abcdef";

struct NyashcaToken *mewie_get_token(void) {
	static int prev_char = 0, current_char = 0;
	
	struct NyashcaToken *token;
	
	if ((token = malloc(sizeof(struct NyashcaToken))) == NULL) {
		mewie_die(strerror(errno));
	}
	
	while (true) {
		if (prev_char) {
			current_char = prev_char;
			prev_char = 0;
		} else {
			current_char = mewie_getc();
		}
		
		switch (current_char) {
		case MEWIE_CHAR_NULL:
			mewie_die("unexpected null character");
		case MEWIE_CHAR_EOF:
			free(token);
			token = NULL;
			
			return token;
		case MEWIE_CHAR_BACKSLASH:
			current_char = mewie_getc();
			
			if (
				current_char == MEWIE_CHAR_LF ||
				
				(
					current_char == MEWIE_CHAR_CR &&
					(current_char = mewie_getc()) == MEWIE_CHAR_LF
				)
			) {
				continue;
			}
			
			mewie_die("unexpected garbage after backslash");
		case MEWIE_CHAR_SPACE: // FALLTHROUGH
		case MEWIE_CHAR_TAB: // FALLTHROUGH
		case MEWIE_CHAR_CR:
			continue;
		case MEWIE_CHAR_HASH:
			while (current_char = mewie_getc(),
				current_char != MEWIE_CHAR_EOF &&
				current_char != MEWIE_CHAR_LF
			) {}
			
			prev_char = current_char;
			
			continue;
		case MEWIE_CHAR_LF:
			token->type = MEWIE_TOKEN_LINE_BREAK;
			
			return token;
		case MEWIE_CHAR_SEMICOLON:
			token->type = MEWIE_TOKEN_EXPRESSION_END;
			
			return token;
		case MEWIE_CHAR_OPENING_PARENTHESIS:
			token->type = MEWIE_TOKEN_INLINE_EXPRESSION_START;
			
			return token;
		case MEWIE_CHAR_CLOSING_PARENTHESIS:
			token->type = MEWIE_TOKEN_INLINE_EXPRESSION_END;
			
			return token;
		case MEWIE_CHAR_QUOTE:
			token->type = MEWIE_TOKEN_QUOTE;
			
			return token;
		case MEWIE_CHAR_DOUBLE_QUOTE: {
			char *string_buffer;
			
			size_t string_buffer_length = 0;
			
			if ((string_buffer = malloc(sizeof(char))) == NULL) {
				mewie_die(strerror(errno));
			}
			
			string_buffer[0] = '\0';
			
			while ((current_char = mewie_getc()) != MEWIE_CHAR_DOUBLE_QUOTE) {
				switch (current_char) {
				case MEWIE_CHAR_EOF:
					mewie_die("unexpected end of file");
				case MEWIE_CHAR_BACKSLASH:
					current_char = mewie_getc();
					
					switch (current_char) {
					case MEWIE_CHAR_EOF:
						mewie_die("unexpected end of file");
					case MEWIE_CHAR_ESCAPE_FROM_CR:
						string_buffer[string_buffer_length] = MEWIE_CHAR_CR;
						
						break;
					case MEWIE_CHAR_ESCAPE_FROM_LF:
						string_buffer[string_buffer_length] = MEWIE_CHAR_LF;
						
						break;
					case MEWIE_CHAR_ESCAPE_FROM_TAB:
						string_buffer[string_buffer_length] = MEWIE_CHAR_TAB;
						
						break;
					case MEWIE_CHAR_ESCAPE_FROM_CHAR: {
						int ch = 0;
						
						for (size_t i = 0; i < MEWIE_CHAR_DIGITS_COUNT; i++) {
							char *digit;
							
							if ((digit = strchr(MEWIE_WORD_HEX_DIGITS,
								tolower(current_char = mewie_getc())
							)) == NULL) {
								mewie_die("invalid hex digit");
							}
							
							ch +=
								(digit - MEWIE_WORD_HEX_DIGITS) <<
								((MEWIE_CHAR_DIGITS_COUNT - (i + 1)) * 4);
						}
						
						if (ch > UCHAR_MAX || ch < 0) {
							mewie_die("couldn't think of such a character");
						}
						
						string_buffer[string_buffer_length] = (char) ch;
						
						break;
					}
					default:
						string_buffer[string_buffer_length] = current_char;
						
						break;
					}
					
					break;
				default:
					string_buffer[string_buffer_length] = current_char;
					
					break;
				}
				
				string_buffer_length++;
				
				if ((string_buffer = realloc(string_buffer,
					(string_buffer_length + 1) * sizeof(char)
				)) == NULL) {
					mewie_die(strerror(errno));
				}
				
				string_buffer[string_buffer_length] = '\0';
			}
			
			token->type = MEWIE_TOKEN_STRING_VALUE;
			
			token->value.as_string.length = string_buffer_length;
			
			token->value.as_string.value = string_buffer;
			
			return token;
		}
		default: {
			char *word_buffer;
			
			size_t word_buffer_length = 0;
			
			if ((word_buffer = malloc(sizeof(char))) == NULL) {
				mewie_die(strerror(errno));
			}
			
			word_buffer[0] = '\0';
			
			while (
				current_char != MEWIE_CHAR_NULL &&
				current_char != MEWIE_CHAR_EOF &&
				current_char != MEWIE_CHAR_BACKSLASH &&
				current_char != MEWIE_CHAR_SPACE &&
				current_char != MEWIE_CHAR_TAB &&
				current_char != MEWIE_CHAR_CR &&
				current_char != MEWIE_CHAR_LF &&
				current_char != MEWIE_CHAR_SEMICOLON &&
				current_char != MEWIE_CHAR_HASH &&
				current_char != MEWIE_CHAR_OPENING_PARENTHESIS &&
				current_char != MEWIE_CHAR_CLOSING_PARENTHESIS &&
				current_char != MEWIE_CHAR_HASH
			) {
				word_buffer[word_buffer_length] = current_char;
				
				word_buffer_length++;
				
				if ((word_buffer = realloc(word_buffer,
					(word_buffer_length + 1) * sizeof(char)
				)) == NULL) {
					mewie_die(strerror(errno));
				}
				
				word_buffer[word_buffer_length] = '\0';
				
				current_char = mewie_getc();
				
				if (current_char == MEWIE_CHAR_NULL) {
					mewie_die("unexpected null character");
				}
			}
			
			prev_char = current_char;
			
			if (
				isdigit(word_buffer[0]) ||
				(
					(
						word_buffer[0] == MEWIE_CHAR_MINUS ||
						word_buffer[0] == MEWIE_CHAR_PLUS
					) &&
					isdigit(word_buffer[1])
				)
			) {
				char *dummy_string = NULL;
				
				long long int int_value = 0;
				double double_value = 0;
				
				int_value = strtoll(word_buffer, &dummy_string, 0);
				
				if (
					dummy_string[0] != '\0' ||
					
					errno == ERANGE ||
					
					int_value > INT64_MAX ||
					int_value < INT64_MIN
				) {
					double_value = strtod(word_buffer, &dummy_string);
					
					if (
						dummy_string[0] != '\0' ||
						
						errno == EINVAL
					) {
						mewie_die("invalid number syntax");
					}
					if (errno == ERANGE) {
						mewie_die("number is too big/small");
					}
					
					token->type = MEWIE_TOKEN_FLOAT_VALUE;
					
					token->value.as_float = double_value;
					
					return token;
				}
				
				token->type = MEWIE_TOKEN_INT_VALUE;
				
				token->value.as_int = (int64_t) int_value;
				
				return token;
			}
			
			if (strcmp(word_buffer, MEWIE_WORD_FUNCTION_START) == 0) {
				token->type = MEWIE_TOKEN_FUNCTION_START;
			} else if (strcmp(word_buffer, MEWIE_WORD_BRANCH_START) == 0) {
				token->type = MEWIE_TOKEN_BRANCH_START;
			} else if (strcmp(word_buffer, MEWIE_WORD_BRANCH_AGAIN) == 0) {
				token->type = MEWIE_TOKEN_BRANCH_AGAIN;
			} else if (strcmp(word_buffer, MEWIE_WORD_BRANCH_OTHERWISE) == 0) {
				token->type = MEWIE_TOKEN_BRANCH_OTHERWISE;
			} else if (strcmp(word_buffer, MEWIE_WORD_LOOP_START) == 0) {
				token->type = MEWIE_TOKEN_LOOP_START;
			} else if (strcmp(word_buffer, MEWIE_WORD_BLOCK_END) == 0) {
				token->type = MEWIE_TOKEN_BLOCK_END;
			} else {
				token->type = MEWIE_TOKEN_SYMBOL_VALUE;
				
				token->value.as_string.length = word_buffer_length;
				
				token->value.as_string.value = word_buffer;
			}
			
			return token;
		}
		}
	}
}

void mewie_print_token(struct NyashcaToken *token) {
	switch (token->type) {
	case MEWIE_TOKEN_LINE_BREAK:
		printf("Line break\n");
		
		break;
	case MEWIE_TOKEN_EXPRESSION_END:
		printf("Expression end\n");
		
		break;
	case MEWIE_TOKEN_INLINE_EXPRESSION_START:
		printf("Inline expression start\n");
		
		break;
	case MEWIE_TOKEN_INLINE_EXPRESSION_END:
		printf("Inline expression end\n");
		
		break;
	case MEWIE_TOKEN_QUOTE:
		printf("Quote\n");
		
		break;
	case MEWIE_TOKEN_INT_VALUE:
		printf("Int value: %" PRId64 "\n", token->value.as_int);
		
		break;
	case MEWIE_TOKEN_FLOAT_VALUE:
		printf("Float value: %lf\n", token->value.as_float);
		
		break;
	case MEWIE_TOKEN_STRING_VALUE:
		printf("String value: %s\n", token->value.as_string.value);
		
		break;
	case MEWIE_TOKEN_SYMBOL_VALUE:
		printf("Symbol value: %s\n", token->value.as_string.value);
		
		break;
	case MEWIE_TOKEN_FUNCTION_START:
		printf("Function start\n");
		
		break;
	case MEWIE_TOKEN_BRANCH_START:
		printf("Branch start\n");
		
		break;
	case MEWIE_TOKEN_BRANCH_AGAIN:
		printf("Branch again\n");
		
		break;
	case MEWIE_TOKEN_BRANCH_OTHERWISE:
		printf("Branch otherwise\n");
		
		break;
	case MEWIE_TOKEN_LOOP_START:
		printf("Loop start\n");
		
		break;
	case MEWIE_TOKEN_BLOCK_END:
		printf("Block end\n");
		
		break;
	default:
		printf("Invalid token\n");
		
		break;
	}
}
