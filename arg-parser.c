#include "mewie.h"

int opterr = 0;

char *MEWIE_STDOUT_NAME = "(stdout)";
char *MEWIE_STDIN_NAME = "(stdin)";

FILE *mewie_current_in_file = NULL;
char *mewie_current_in_file_name = NULL;

uint64_t mewie_current_line = 1;
uint64_t mewie_current_column = 0;

void mewie_parse_args(int *argc, char ***argv) {
	int current_char = 0;
	
	while ((current_char = getopt(*argc, *argv, "cvo:s:")) != -1) {
		switch (current_char) {
		case 'v':
			fprintf(stderr, "%s %s\n", mewie_name, mewie_version);
			exit(EXIT_SUCCESS);
			
			break;
		default:
			fprintf(stderr, "usage: %s %s\n", mewie_name, mewie_usage);
			exit(EXIT_FAILURE);
			
			break;
		}
	}
	
	*argc -= optind;
	*argv += optind;
}

FILE *mewie_open_in_file(int *argc, char ***argv) {
	if (
		mewie_current_in_file != NULL &&
		mewie_current_in_file != stdin
	) {
		if (fclose(mewie_current_in_file) == EOF) {
			fprintf(stderr,
				"%s: error: %s: %s\n",
			mewie_name, mewie_current_in_file_name, strerror(errno));
			exit(EXIT_FAILURE);
		}
	}
	
	if (*argc < 1) {
		if (mewie_current_in_file == NULL) {
			fprintf(stderr, "%s: error: no input files\n", mewie_name);
			exit(EXIT_FAILURE);
		}
		
		mewie_current_in_file = NULL;
		mewie_current_in_file_name = NULL;
		
		return mewie_current_in_file;
	}
	
	if (strcmp(*argv[0], "-") == 0) {
		mewie_current_in_file = stdin;
		mewie_current_in_file_name = MEWIE_STDIN_NAME;
	} else {
		if ((mewie_current_in_file = fopen(*argv[0], "r")) == NULL) {
			fprintf(stderr,
				"%s: error: %s: %s\n",
			mewie_name, *argv[0], strerror(errno));
			exit(EXIT_FAILURE);
		}
		mewie_current_in_file_name = *argv[0];
	}
	
	(*argc)--;
	(*argv)++;
	
	mewie_current_line = 1;
	mewie_current_column = 0;
	
	return mewie_current_in_file;
}

int mewie_getc(void) {
	int ch = fgetc(mewie_current_in_file);
	
	if (ch == '\n') {
		mewie_current_line++;
		mewie_current_column = 0;
	} else {
		mewie_current_column++;
	}
	
	return ch;
}
