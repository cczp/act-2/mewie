#include "mewie.h"

const char *mewie_name = "mewie";
const char *mewie_version = "0.0.1-prealpha";
const char *mewie_usage = "[-v] [in-file ...]";

int main(int argc, char **argv) {
	mewie_parse_args(&argc, &argv);
	
	while(mewie_open_in_file(&argc, &argv) != NULL) {
		// TODO: remove this shit
		printf("%s\n\n", mewie_current_in_file_name);
		
		struct NyashcaNode *current_node;
		while ((current_node = mewie_get_node()) != NULL) {
			mewie_print_node(current_node, 0);
		}
		
		printf("\n");
		//
	}
	
	return EXIT_SUCCESS;
}

void mewie_die(char *last_words) {
	printf("%s: error: %s: %zu: %zu: %s\n",
		mewie_name,
		
		mewie_current_in_file_name,
		
		mewie_current_line,
		mewie_current_column,
		
		last_words
	);
	exit(EXIT_FAILURE);
}
